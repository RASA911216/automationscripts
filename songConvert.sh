#!/bin/bash
MP4FILE=$(ls ~/Desktop/SONGS/ |grep .mp4)
for filename in $MP4FILE
do 
 name=`echo "$filename" | sed -e "s/.mp4$//g"`
 ffmpeg -i ~/Desktop/SONGS/$filename -b:a 192K -vn ~/Desktop/SONGS/$name.mp3
done